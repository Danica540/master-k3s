# Base benchmark results

# MySQL benchmarks

## Test 1
```bash
sysbench oltp_read_only --threads=5 --mysql-host=node01.k3s-nodes.danica.dacha204.com --mysql-user=root --mysql-password=<hidden>--mysql-port=33060 --tables=10 --table-size=100000 prepare
```

Sysbench tests:
1. oltp_read_only.lua
2. oltp_read_write.lua
3. oltp_point_select.lua
4. select_random_ranges.lua

### 3 mysql: direct mysql instance

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12096
        write:                           0
        other:                           1728
        total:                           13824
    transactions:                        864    (85.82 per sec.)
    queries:                             13824  (1373.12 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0656s
    total number of events:              864

Latency (ms):
         min:                                   27.06
         avg:                                   58.08
         max:                                  163.08
         95th percentile:                       95.81
         sum:                                50181.15

Threads fairness:
    events (avg/stddev):           172.8000/0.40
    execution time (avg/stddev):   10.0362/0.02
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12922
        write:                           0
        other:                           1846
        total:                           14768
    transactions:                        923    (91.95 per sec.)
    queries:                             14768  (1471.20 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0354s
    total number of events:              923

Latency (ms):
         min:                                   28.41
         avg:                                   54.26
         max:                                  162.14
         95th percentile:                       87.56
         sum:                                50079.58

Threads fairness:
    events (avg/stddev):           184.6000/0.80
    execution time (avg/stddev):   10.0159/0.01

```

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            11942
        write:                           0
        other:                           1706
        total:                           13648
    transactions:                        853    (84.19 per sec.)
    queries:                             13648  (1347.10 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.1260s
    total number of events:              853

Latency (ms):
         min:                                   30.39
         avg:                                   58.99
         max:                                  177.25
         95th percentile:                       86.00
         sum:                                50321.08

Threads fairness:
    events (avg/stddev):           170.6000/0.49
    execution time (avg/stddev):   10.0642/0.04

```

### 3 mysql: direct proxysql instance

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            14028
        write:                           0
        other:                           2004
        total:                           16032
    transactions:                        1002   (99.88 per sec.)
    queries:                             16032  (1598.09 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0270s
    total number of events:              1002

Latency (ms):
         min:                                   26.19
         avg:                                   49.96
         max:                                  285.60
         95th percentile:                       74.46
         sum:                                50058.47

Threads fairness:
    events (avg/stddev):           200.4000/3.83
    execution time (avg/stddev):   10.0117/0.01

```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            13832
        write:                           0
        other:                           1976
        total:                           15808
    transactions:                        988    (98.31 per sec.)
    queries:                             15808  (1573.04 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0441s
    total number of events:              988

Latency (ms):
         min:                                   24.22
         avg:                                   50.73
         max:                                  127.43
         95th percentile:                       84.47
         sum:                                50117.88

Threads fairness:
    events (avg/stddev):           197.6000/0.49
    execution time (avg/stddev):   10.0236/0.02
```

```try1
sysbench 1.0.20 (using bundled LuaJIT 2.1.0-beta2)

Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12558
        write:                           0
        other:                           1794
        total:                           14352
    transactions:                        897    (89.30 per sec.)
    queries:                             14352  (1428.75 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0395s
    total number of events:              897

Latency (ms):
         min:                                   27.77
         avg:                                   55.84
         max:                                  312.82
         95th percentile:                       97.55
         sum:                                50086.34

Threads fairness:
    events (avg/stddev):           179.4000/1.50
    execution time (avg/stddev):   10.0173/0.01
```

### 3 mysql: proxysql service

```try3

Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            13328
        write:                           0
        other:                           1904
        total:                           15232
    transactions:                        952    (94.53 per sec.)
    queries:                             15232  (1512.54 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0653s
    total number of events:              952

Latency (ms):
         min:                                   28.80
         avg:                                   52.72
         max:                                  168.34
         95th percentile:                       81.48
         sum:                                50189.89

Threads fairness:
    events (avg/stddev):           190.4000/2.42
    execution time (avg/stddev):   10.0380/0.01
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12922
        write:                           0
        other:                           1846
        total:                           14768
    transactions:                        923    (91.75 per sec.)
    queries:                             14768  (1467.94 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0583s
    total number of events:              923

Latency (ms):
         min:                                   27.22
         avg:                                   54.28
         max:                                  130.08
         95th percentile:                       89.16
         sum:                                50104.43

Threads fairness:
    events (avg/stddev):           184.6000/1.02
    execution time (avg/stddev):   10.0209/0.02

```

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12236
        write:                           0
        other:                           1748
        total:                           13984
    transactions:                        874    (86.71 per sec.)
    queries:                             13984  (1387.37 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0761s
    total number of events:              874

Latency (ms):
         min:                                   27.47
         avg:                                   57.42
         max:                                  145.84
         95th percentile:                       94.10
         sum:                                50189.31

Threads fairness:
    events (avg/stddev):           174.8000/0.75
    execution time (avg/stddev):   10.0379/0.02

```



## Test 2
```bash
sysbench oltp_read_write --threads=5 --mysql-host=node01.k3s-nodes.danica.dacha204.com --mysql-user=root --mysql-password=<hidden>--mysql-port=33060 --tables=10 --table-size=100000 prepare
```

### 3 mysql: direct mysql instance
```try1
sysbench 1.0.20 (using bundled LuaJIT 2.1.0-beta2)

Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            8694
        write:                           881
        other:                           2845
        total:                           12420
    transactions:                        621    (61.62 per sec.)
    queries:                             12420  (1232.32 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0748s
    total number of events:              621

Latency (ms):
         min:                                   45.29
         avg:                                   80.85
         max:                                  162.02
         95th percentile:                      118.92
         sum:                                50208.05

Threads fairness:
    events (avg/stddev):           124.2000/0.75
    execution time (avg/stddev):   10.0416/0.02
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            8568
        write:                           982
        other:                           2690
        total:                           12240
    transactions:                        612    (60.65 per sec.)
    queries:                             12240  (1212.93 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0858s
    total number of events:              612

Latency (ms):
         min:                                   46.18
         avg:                                   82.12
         max:                                  192.78
         95th percentile:                      132.49
         sum:                                50256.97

Threads fairness:
    events (avg/stddev):           122.4000/0.49
    execution time (avg/stddev):   10.0514/0.02
```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9114
        write:                           1127
        other:                           2779
        total:                           13020
    transactions:                        651    (64.79 per sec.)
    queries:                             13020  (1295.75 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0444s
    total number of events:              651

Latency (ms):
         min:                                   41.12
         avg:                                   77.01
         max:                                  192.23
         95th percentile:                      116.80
         sum:                                50134.52

Threads fairness:
    events (avg/stddev):           130.2000/0.40
    execution time (avg/stddev):   10.0269/0.01
```
### 3 mysql instances: direct proxysql

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9520
        write:                           989
        other:                           3091
        total:                           13600
    transactions:                        680    (67.03 per sec.)
    queries:                             13600  (1340.54 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.1393s
    total number of events:              680

Latency (ms):
         min:                                   36.56
         avg:                                   73.96
         max:                                  232.54
         95th percentile:                      123.28
         sum:                                50293.88

Threads fairness:
    events (avg/stddev):           136.0000/17.92
    execution time (avg/stddev):   10.0588/0.05

```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            8876
        write:                           1018
        other:                           2786
        total:                           12680
    transactions:                        634    (62.91 per sec.)
    queries:                             12680  (1258.28 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0710s
    total number of events:              634

Latency (ms):
         min:                                   47.56
         avg:                                   79.13
         max:                                  433.33
         95th percentile:                      123.28
         sum:                                50167.47

Threads fairness:
    events (avg/stddev):           126.8000/3.54
    execution time (avg/stddev):   10.0335/0.02
```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9604
        write:                           1201
        other:                           2915
        total:                           13720
    transactions:                        686    (67.81 per sec.)
    queries:                             13720  (1356.10 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.1112s
    total number of events:              686

Latency (ms):
         min:                                   40.58
         avg:                                   73.32
         max:                                  259.35
         95th percentile:                      125.52
         sum:                                50300.32

Threads fairness:
    events (avg/stddev):           137.2000/18.82
    execution time (avg/stddev):   10.0601/0.03

```

### 3 mysql: proxysql service

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            8946
        write:                           948
        other:                           2884
        total:                           12778
    transactions:                        638    (62.83 per sec.)
    queries:                             12778  (1258.34 per sec.)
    ignored errors:                      1      (0.10 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.1493s
    total number of events:              638

Latency (ms):
         min:                                   43.27
         avg:                                   79.32
         max:                                  280.52
         95th percentile:                      123.28
         sum:                                50609.12

Threads fairness:
    events (avg/stddev):           127.6000/12.11
    execution time (avg/stddev):   10.1218/0.02

```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9492
        write:                           1107
        other:                           2961
        total:                           13560
    transactions:                        678    (67.31 per sec.)
    queries:                             13560  (1346.19 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0696s
    total number of events:              678

Latency (ms):
         min:                                   41.94
         avg:                                   74.00
         max:                                  193.05
         95th percentile:                      112.67
         sum:                                50171.53

Threads fairness:
    events (avg/stddev):           135.6000/10.56
    execution time (avg/stddev):   10.0343/0.02
```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9380
        write:                           1155
        other:                           2865
        total:                           13400
    transactions:                        670    (66.47 per sec.)
    queries:                             13400  (1329.48 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0752s
    total number of events:              670

Latency (ms):
         min:                                   40.42
         avg:                                   74.94
         max:                                  283.82
         95th percentile:                      118.92
         sum:                                50211.68

Threads fairness:
    events (avg/stddev):           134.0000/12.87
    execution time (avg/stddev):   10.0423/0.02
```


## Test 3
```bash
sysbench oltp_point_select --threads=5 --mysql-host=node01.k3s-nodes.danica.dacha204.com --mysql-user=root --mysql-password=<hidden>--mysql-port=33060 --tables=10 --table-size=100000 prepare
```

### 3 mysql: direct mysql instance

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            14548
        write:                           0
        other:                           0
        total:                           14548
    transactions:                        14548  (1450.67 per sec.)
    queries:                             14548  (1450.67 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0245s
    total number of events:              14548

Latency (ms):
         min:                                    1.17
         avg:                                    3.44
         max:                                   45.13
         95th percentile:                        7.43
         sum:                                50054.87

Threads fairness:
    events (avg/stddev):           2909.6000/6.28
    execution time (avg/stddev):   10.0110/0.00
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            14342
        write:                           0
        other:                           0
        total:                           14342
    transactions:                        14342  (1431.56 per sec.)
    queries:                             14342  (1431.56 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0136s
    total number of events:              14342

Latency (ms):
         min:                                    1.21
         avg:                                    3.49
         max:                                   37.93
         95th percentile:                        7.84
         sum:                                49996.95

Threads fairness:
    events (avg/stddev):           2868.4000/9.65
    execution time (avg/stddev):   9.9994/0.00

```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            14478
        write:                           0
        other:                           0
        total:                           14478
    transactions:                        14478  (1445.31 per sec.)
    queries:                             14478  (1445.31 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0144s
    total number of events:              14478

Latency (ms):
         min:                                    1.17
         avg:                                    3.45
         max:                                   25.33
         95th percentile:                        7.70
         sum:                                49977.96

Threads fairness:
    events (avg/stddev):           2895.6000/14.51
    execution time (avg/stddev):   9.9956/0.00

```

### 3 mysql: direct proxysql instance

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            15182
        write:                           0
        other:                           0
        total:                           15182
    transactions:                        15182  (1516.38 per sec.)
    queries:                             15182  (1516.38 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0079s
    total number of events:              15182

Latency (ms):
         min:                                    1.04
         avg:                                    3.29
         max:                                   64.21
         95th percentile:                        6.91
         sum:                                49972.63

Threads fairness:
    events (avg/stddev):           3036.4000/12.03
    execution time (avg/stddev):   9.9945/0.00

```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            15773
        write:                           0
        other:                           0
        total:                           15773
    transactions:                        15773  (1575.53 per sec.)
    queries:                             15773  (1575.53 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0061s
    total number of events:              15773

Latency (ms):
         min:                                    1.04
         avg:                                    3.17
         max:                                   62.34
         95th percentile:                        6.32
         sum:                                49974.95

Threads fairness:
    events (avg/stddev):           3154.6000/22.88
    execution time (avg/stddev):   9.9950/0.00

```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            16166
        write:                           0
        other:                           0
        total:                           16166
    transactions:                        16166  (1614.13 per sec.)
    queries:                             16166  (1614.13 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0115s
    total number of events:              16166

Latency (ms):
         min:                                    0.99
         avg:                                    3.09
         max:                                   67.22
         95th percentile:                        6.21
         sum:                                49988.98

Threads fairness:
    events (avg/stddev):           3233.2000/16.38
    execution time (avg/stddev):   9.9978/0.00

```

### 3 mysql: proxysql service

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            15499
        write:                           0
        other:                           0
        total:                           15499
    transactions:                        15499  (1547.17 per sec.)
    queries:                             15499  (1547.17 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0148s
    total number of events:              15499

Latency (ms):
         min:                                    1.17
         avg:                                    3.23
         max:                                   35.95
         95th percentile:                        7.30
         sum:                                50032.10

Threads fairness:
    events (avg/stddev):           3099.8000/4.31
    execution time (avg/stddev):   10.0064/0.00
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            15361
        write:                           0
        other:                           0
        total:                           15361
    transactions:                        15361  (1533.97 per sec.)
    queries:                             15361  (1533.97 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0103s
    total number of events:              15361

Latency (ms):
         min:                                    1.17
         avg:                                    3.25
         max:                                   34.40
         95th percentile:                        7.30
         sum:                                49990.02

Threads fairness:
    events (avg/stddev):           3072.2000/13.36
    execution time (avg/stddev):   9.9980/0.00
```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            15148
        write:                           0
        other:                           0
        total:                           15148
    transactions:                        15148  (1512.74 per sec.)
    queries:                             15148  (1512.74 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0088s
    total number of events:              15148

Latency (ms):
         min:                                    1.18
         avg:                                    3.30
         max:                                   83.11
         95th percentile:                        7.04
         sum:                                49992.27

Threads fairness:
    events (avg/stddev):           3029.6000/9.81
    execution time (avg/stddev):   9.9985/0.00

```

## Test 4
```bash
sysbench select_random_ranges --threads=5 --mysql-host=node01.k3s-nodes.danica.dacha204.com --mysql-user=root --mysql-password=<hidden>--mysql-port=33060 --tables=10 --table-size=100000 prepare
```

### 3 mysql: direct mysql instance

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9294
        write:                           0
        other:                           0
        total:                           9294
    transactions:                        9294   (928.40 per sec.)
    queries:                             9294   (928.40 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0058s
    total number of events:              9294

Latency (ms):
         min:                                    1.50
         avg:                                    5.38
         max:                                   33.25
         95th percentile:                       11.87
         sum:                                49964.84

Threads fairness:
    events (avg/stddev):           1858.8000/191.99
    execution time (avg/stddev):   9.9930/0.00

```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9665
        write:                           0
        other:                           0
        total:                           9665
    transactions:                        9665   (965.44 per sec.)
    queries:                             9665   (965.44 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0063s
    total number of events:              9665

Latency (ms):
         min:                                    1.37
         avg:                                    5.17
         max:                                   32.24
         95th percentile:                       10.46
         sum:                                49965.60

Threads fairness:
    events (avg/stddev):           1933.0000/203.07
    execution time (avg/stddev):   9.9931/0.00

```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            9763
        write:                           0
        other:                           0
        total:                           9763
    transactions:                        9763   (975.25 per sec.)
    queries:                             9763   (975.25 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0064s
    total number of events:              9763

Latency (ms):
         min:                                    1.54
         avg:                                    5.12
         max:                                  210.09
         95th percentile:                       10.27
         sum:                                49963.60

Threads fairness:
    events (avg/stddev):           1952.6000/206.20
    execution time (avg/stddev):   9.9927/0.00

```

### 3 mysql: direct proxysql instance

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12410
        write:                           0
        other:                           0
        total:                           12410
    transactions:                        12410  (1239.43 per sec.)
    queries:                             12410  (1239.43 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0083s
    total number of events:              12410

Latency (ms):
         min:                                    1.42
         avg:                                    4.03
         max:                                   61.15
         95th percentile:                        8.90
         sum:                                49964.34

Threads fairness:
    events (avg/stddev):           2482.0000/611.08
    execution time (avg/stddev):   9.9929/0.00
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            13331
        write:                           0
        other:                           0
        total:                           13331
    transactions:                        13331  (1331.62 per sec.)
    queries:                             13331  (1331.62 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0070s
    total number of events:              13331

Latency (ms):
         min:                                    1.43
         avg:                                    3.75
         max:                                   73.72
         95th percentile:                        8.28
         sum:                                49966.81

Threads fairness:
    events (avg/stddev):           2666.2000/678.37
    execution time (avg/stddev):   9.9934/0.00
```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            11933
        write:                           0
        other:                           0
        total:                           11933
    transactions:                        11933  (1191.80 per sec.)
    queries:                             11933  (1191.80 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0072s
    total number of events:              11933

Latency (ms):
         min:                                    1.43
         avg:                                    4.19
         max:                                  116.43
         95th percentile:                        9.06
         sum:                                49961.78

Threads fairness:
    events (avg/stddev):           2386.6000/582.81
    execution time (avg/stddev):   9.9924/0.00
```
### 3 mysql: proxysql service

```try1
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12074
        write:                           0
        other:                           0
        total:                           12074
    transactions:                        12074  (1206.42 per sec.)
    queries:                             12074  (1206.42 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0050s
    total number of events:              12074

Latency (ms):
         min:                                    1.38
         avg:                                    4.14
         max:                                   45.12
         95th percentile:                        9.39
         sum:                                49959.96

Threads fairness:
    events (avg/stddev):           2414.8000/500.66
    execution time (avg/stddev):   9.9920/0.00
```

```try2
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            11927
        write:                           0
        other:                           0
        total:                           11927
    transactions:                        11927  (1191.12 per sec.)
    queries:                             11927  (1191.12 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0084s
    total number of events:              11927

Latency (ms):
         min:                                    1.33
         avg:                                    4.19
         max:                                   31.82
         95th percentile:                        9.56
         sum:                                49973.99

Threads fairness:
    events (avg/stddev):           2385.4000/510.94
    execution time (avg/stddev):   9.9948/0.00
```

```try3
Running the test with following options:
Number of threads: 5
Initializing random number generator from current time


Initializing worker threads...

Threads started!

SQL statistics:
    queries performed:
        read:                            12031
        write:                           0
        other:                           0
        total:                           12031
    transactions:                        12031  (1202.51 per sec.)
    queries:                             12031  (1202.51 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          10.0031s
    total number of events:              12031

Latency (ms):
         min:                                    1.32
         avg:                                    4.15
         max:                                   28.77
         95th percentile:                        9.39
         sum:                                49957.05

Threads fairness:
    events (avg/stddev):           2406.2000/487.59
    execution time (avg/stddev):   9.9914/0.00
```
