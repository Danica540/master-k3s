# Deployment
```bash
cd k3s-setup
cd chaos-mesh
kubectl create -f chaos-mesh.crds.yaml # create se koristi jer je fajl prevelik za smestanje u anotacije (kubectl.kubernetess.io/last-applied-configurations), a apply automatski kreira te anotacije zbog undo feature-a
cd ..
kubectl apply -k .
```